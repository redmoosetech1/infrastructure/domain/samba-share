datacenter = "Datacenter"
datastore_1 = "esx0_local_storage"
datastore_2 = "Storage"
pool = "Resources"
network = "Domain"
template = "rocky8.5_server_amd64_template"
folder = "Infrastructure/Production/Domain"
dns_domain = "rmt"
dns_servers = ["172.16.20.200", "172.16.20.201"]
machines = {
 share = {
  hostname = "share"
  domain = "rmt.local"
  ipv4_address = "172.16.20.202"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.20.254"
  num_cpus = 4
  num_cores_per_socket = 4
  memory = 4096
  disks = [
   {
     label = "disk1"
     size = 4096
     unit_number = 1
   }
  ]
 }
}

