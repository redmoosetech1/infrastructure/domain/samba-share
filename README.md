# Samba AD-DC

# Manual Install
Run the playbook named samba.yml. You need to set vault environment variables first.
```bash
$ VAULT_ADDR="hostname or ip address" \
VAULT_TOKEN="Admin Token" \
ansible-playbook samba.yml
```
Configure Kerberos
/etc/krb5.conf
```
[libdefaults]
	default_realm = RMT.LOCAL
	dns_lookup_realm = false
	dns_lookup_kdc = true
```

Configure Time
```
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See timesyncd.conf(5) for details.

[Time]
NTP=172.16.20.200
#FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
#RootDistanceMaxSec=5
#PollIntervalMinSec=32
#PollIntervalMaxSec=2048
```

Configure Samba File
```
[global]
       security = ADS
       workgroup = RMT
       realm = RMT.LOCAL
       username map = /etc/samba/user.map

       log file = /var/log/samba/%m.log
       log level = 1

       # Default ID mapping configuration using the rid
       # idmap backend. This will work out of the box for simple setups
       # as well as complex setups with trusted domains.
       idmap config * : backend = tdb
       idmap config * : range = 3000-7999
       idmap config RMT : backend = rid
       idmap config RMT : range = 10000-9999999
       
       # Template settings for login shell and home directory
       template shell = /bin/bash
       template homedir = /home/%U
```

Create User Map
```
!root = RMT\Administrator
```

After the machine is up, you need to ssh into the machine and join the server to the domain.
```bash
$ net ads join -U administrator
```

Configure Name Service Switch
/etc/nsswitch.conf
```
passwd: files winbind
group:  files winbind
```

Start the services
```bash
$ systemctl enable nmb.service smb.service winbind.service
$ systemctl start nmb.service smb.service winbind.service
```
